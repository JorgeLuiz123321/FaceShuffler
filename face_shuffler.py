import sys
import dlib
import numpy
import cv2

PREDICTOR_FILE = './shape_predictor_68_face_landmarks.dat'

MASK_GROUPS = [
    list(range(17, 27)) + list(range(36, 48)), # Olhos e obrancelhas
    list(range(31, 36)) + [21, 22] + [49, 53], # Nariz
    list(range(48, 61))                        # Boca
]


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(PREDICTOR_FILE)

def find_faces(img):
    # Localiza regiões contendo faces
    detections = detector(img, 1)
    # Retorna uma lista com os 'predictors' de cada face
    return tuple((predictor(img, region) for region in detections))

# Transforma um 'predictor' de uma face em uma matriz de pontos-chave
def get_position_matrix(face):
    points = ((point.x, point.y) for point in face.parts())
    points = tuple(points)
    return numpy.asmatrix(points).astype(numpy.float32)

# Calcula a matrix de tranformação da face_b para a face_a
def get_transformation(face_a, face_b):
    # Copia para preservar as coordenadas originais das faces
    face_a = numpy.asmatrix(numpy.copy(face_a))
    face_b = numpy.asmatrix(numpy.copy(face_b))

    # Calcula os centroides
    centroid_a = numpy.mean(face_a, axis=0)
    centroid_b = numpy.mean(face_b, axis=0)

    # Centraliza as os pontos em volta da origem
    face_a -= centroid_a
    face_b -= centroid_b

    # Usa o desvio padrão para igualar a escala
    std_a = numpy.std(face_a)
    std_b = numpy.std(face_b)
    face_a /= std_a
    face_b /= std_b

    # Here be deep magic. (http://www.catb.org/jargon/html/D/deep-magic.html)
    U, S, Vt = numpy.linalg.svd(face_a.T * face_b)
    R = (U * Vt).T
    return numpy.vstack((numpy.hstack((
        (std_b / std_a) * R,
        centroid_b.T - (std_b / std_a) * R * centroid_a.T)),
        numpy.matrix((0, 0, 1)).astype(numpy.float32)
    ))

# Aplica a transformação M na imagem img
def apply_transformation(img, M, shape = None):
    shape = shape or img.shape
    output = numpy.zeros(shape, dtype=img.dtype)
    cv2.warpAffine(img, M,
        (shape[1], shape[0]), # OpenCV usa coordenadas invertidas
        dst = output,
        flags = cv2.WARP_INVERSE_MAP | cv2.INTER_LANCZOS4)
    return numpy.maximum(numpy.minimum(output, 255), 0)

def fill_convex_region(img, region):
    points = cv2.convexHull(region)[:,0]
    cv2.fillConvexPoly(img, points, color = 1)

# Cria uma máscara para aplicar sobre a imagem transformada
def build_mask(face, shape):
    # Cria uma mascara vazia
    mask = numpy.zeros(shape, dtype=numpy.float32)

    # Pra cada região da mascara
    for region in MASK_GROUPS:
        # Calcula e preenche um poligono convexo da região
        poly = cv2.convexHull(face[region])[:,0].astype(int)
        cv2.fillConvexPoly(mask, poly, (1,1,1))

    # Dilata a mascara gerada antes de suvizar as bordas para preservar melhor
    # o tamanho da mascara
    mask = cv2.dilate(mask, numpy.ones((3,3)), iterations=3)
    mask = cv2.GaussianBlur(mask * 255, (13, 13), 0)

    # Normaliza antes de retornar
    mask -= numpy.min(mask)
    mask /= numpy.max(mask)
    return mask

# Aplica 'foreground' sobre 'background' usando a mascara 'alpha'
def alpha_blend(background, foreground, alpha):
    background *= (1 - alpha)
    background += foreground * alpha

# k: deve ser escolhido de acordo com o tamanho relativo da face na imagem.
# r: valores maiores preservam mais os detalhes da imagem target, mas reduz
# a qualidade da correção de cor
def color_correct(original, target, k, r = 1.2):
    # Ajusta o tamanho de k, que deve ser um inteiro ímpar
    k = int(k * r)
    if k % 2 == 0: k += 1

    # Calcula versÕes borradas de ambas imagens
    blur_original = cv2.GaussianBlur(original, (k, k), 0)
    blur_target = cv2.GaussianBlur(target, (k, k), 0)

    # Evita divisão por zero
    blur_target += 1 * (blur_target <= 1)

    # Faz a correção de cor multiplicando a imagem alvo pela razão entre
    # as imagens borradas, suavisando as diferenças de cores entre elas
    corrected = target * blur_original / blur_target

    # Recupera detalhes da imagem corrigida através de um 'shrpen'
    cv2.addWeighted(corrected, 1.2, blur_original, -0.2, 0, corrected)

    # Evita valores fora do range correto
    return numpy.maximum(numpy.minimum(corrected, 255), 0)

# Copia uma face 'source' de 'source_img' sobre a face 'target' na 'target_img'
def copy_face(source_img, target_img, target, source):
    # Calcula a transformação para alinhar de uma face para a outra
    T = get_transformation(target, source)[:2]

    # Alica a transformação na imagem original
    transformed = apply_transformation(source_img, T, target_img.shape)

    # Correção de cor, usa a tamanho relativo da face como referencia para o
    # tamanho do kernel do GaussianBlur
    k = numpy.linalg.norm(target[37] - target[44]) # Distãncia entre os olhos
    col_corected = color_correct(source_img, transformed, k)

    # Calcula a mascara combinado mascaras das duas faces
    mask = build_mask(source, target_img.shape)
    mask = apply_transformation(mask, T)
    mask = numpy.maximum(mask, build_mask(target, target_img.shape))

    # Sobrepõem as imagens usando alpha blendeing
    alpha_blend(target_img, col_corected, mask)

def random_pairs(n):
    # TODO Evitar que pares iguais sejam gerados
    rand = numpy.array(tuple(range(n)))
    numpy.random.shuffle(rand)
    return tuple(((x, y) for x, y in enumerate(rand)))

if __name__ == '__main__':
    # Carrega a imagem
    img = dlib.load_rgb_image(sys.argv[1])

    # Detecta as faces
    faces = find_faces(img)

    # Converte para float para processar e faz uma cópia para o resultado
    img = img.astype(numpy.float32)
    output = numpy.copy(img)

    for pair in random_pairs(len(faces)):
        # Escolhe as posições de duas faces
        source = get_position_matrix(faces[pair[0]])
        target = get_position_matrix(faces[pair[1]])

        # Troca as faces
        copy_face(img, output, source, target)

    # Salva a imagem final
    dlib.save_image(output.astype(numpy.uint8), 'shuffled.png')

    # Cria uma janela com a imagem
    # win = dlib.image_window()
    # win.clear_overlay()
    # win.set_image(output.astype(numpy.uint8))
    # dlib.hit_enter_to_continue()
