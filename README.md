# Face Shuffler
Este programa embaralha os rostos contidos em uma foto.

## Dependências
As dependências necessárias podem ser instaladas com: `pip install dlib opencv-python numpy`

## Uso
Para executar basta rodar o seguinte comando: `python face_shuffler.py`.

## Exemplos
